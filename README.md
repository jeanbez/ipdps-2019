# Towards the Reconfiguration of the I/O Forwarding Layer

*Jean Luca Bez (1), Francieli Zanon Boito (2), Ramon Nou (4), Alberto Miranda (4), Toni Cortes (3), and Philippe O. A. Navaux (1)*

1. Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil

2. Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France

3. Barcelona Supercomputing Center (BSC), Barcelona, Spain

4. Universitat Politècnica de Catalunya, Barcelona, Spain
